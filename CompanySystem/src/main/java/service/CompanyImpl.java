package service;

import model.Employee;

import java.util.List;
import java.util.Scanner;

public class CompanyImpl {

    public void simpleStats(List<Employee> employeeList) {

        int id=0;
        double sum=0;

        System.out.println("Give your choice. \n\r1: Print Employees" + "\n\r2: Company Yearly Expenses"
                            + "\n\r3: Hire an employee" + "\n\r4: Fire an employee" + "\n\r5: Promote an employee");
        Scanner scanner = new Scanner(System.in);
        int choice = scanner.nextInt();

        switch (choice){
            case 1:
                for (Employee employee : employeeList){
                    id++;
                    employee.calcSalary();
                    System.out.println("id ="+id+" "+ employee.toString());
                }
            break;

            case 2:
                for (Employee employee : employeeList){
                    employee.calcSalary();
                    sum += employee.getSalary();
                }
                System.out.println("Company Year Expenses: "+(12*sum));
            break;

            case 3:
                System.out.println("Please enter first name:");
                Scanner fn = new Scanner(System.in);
                String fname = fn.nextLine();
                System.out.println("Please enter last name:");
                Scanner ln = new Scanner(System.in);
                String lname = ln.nextLine();
                System.out.println("Please enter salary:");
                Scanner sl = new Scanner(System.in);
                double slr = sl.nextDouble();
                System.out.println("Please enter type:");
                Scanner tp = new Scanner(System.in);
                String type = tp.nextLine();
                System.out.println("Please enter address:");
                Scanner addr = new Scanner(System.in);
                String address = addr.nextLine();
                employeeList.add(new Employee(fname, lname, slr, type, address));

                for (Employee employee : employeeList){
                    id++;
                    employee.calcSalary();
                    System.out.println("id ="+id+" "+ employee.toString());
                }
                break;

            case 4:
                for (Employee employee : employeeList){
                    id++;
                    employee.calcSalary();
                    System.out.println("id ="+id+" "+ employee.toString());
                }
                System.out.println("Give employee's id to fire: ");
                Scanner sc = new Scanner(System.in);
                int idToDelete = sc.nextInt();
                employeeList.remove(idToDelete-1);

                System.out.println("Employee deleted successfully");
                System.out.println(employeeList.toString());
                break;

            case 5:
                for (Employee employee : employeeList){
                    id++;
                    employee.calcSalary();
                    System.out.println("id ="+id+" "+ employee.toString());
                }
                System.out.println("Give employee's id to promote: ");
                Scanner scn = new Scanner(System.in);
                int idToPromote = scn.nextInt();
                employeeList.get(idToPromote-1).setType("Manager");
                System.out.println(employeeList.toString());
                break;
        }
    }
}
