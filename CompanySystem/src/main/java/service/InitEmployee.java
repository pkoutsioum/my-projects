package service;

import model.Employee;

import java.util.ArrayList;
import java.util.List;

public class InitEmployee {
    public static List<Employee> employeeList() {

        List<Employee> employeeList = new ArrayList<Employee>();

        employeeList.add(new Employee("Panagiotis", "Koutsioubas", 1200, "Developer", "G.Botsi 42"));
        employeeList.add(new Employee("Thomas", "Koutsioubas", 1300, "Manager", "Foinikos 12"));
        employeeList.add(new Employee("Sotiris", "Koutsioubas", 1150, "Developer", "Megakleous 37"));
        employeeList.add(new Employee("Nikos", "Nikolaou", 1300, "Manager", "Platanos 8"));

        return employeeList;
    }
}
