package gr.example.ebookstore.service;

import gr.example.ebookstore.model.Book;
import gr.example.ebookstore.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Service;

@Service
public class DBInit implements ApplicationListener<ApplicationReadyEvent> {

    @Autowired
    private BookRepository bookRepository;

    @Override
    public void onApplicationEvent(ApplicationReadyEvent applicationReadyEvent) {
        Book myAwesomeBook = new Book();
        myAwesomeBook.setNumberOfPages(570);
        myAwesomeBook.setAuthor("Zorbas Alexis");
        myAwesomeBook.setTitle("Vios kai Politeia");

        Book myAwesomeBook2 = new Book();
        myAwesomeBook2.setNumberOfPages(350);
        myAwesomeBook2.setAuthor("Lountemis Menelaos");
        myAwesomeBook2.setTitle("Ena paidi metraei t' astra");

        bookRepository.save(myAwesomeBook);
        bookRepository.save(myAwesomeBook2);
    }

}
